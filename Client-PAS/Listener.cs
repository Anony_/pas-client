﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_PAS
{
    public class Listener
    {
        private Socket _socket;
        private Form1 _form;

        public Listener(Socket socket, Form1 form)
        {
            this._socket = socket;
            this._form = form;
        }

        public void Start()
        {
            try
            {
                var buffer = new byte[16384];

                // Getting Welcome message
                _socket.Receive(buffer);
                UpdateSystemLog("You've been connected.");

                Helper.Send(_socket, "", ResponseCode.GET_NAME);

                while (true)
                {
                    var recv = _socket.Receive(buffer);
                    if (recv <= 0) continue;

                    var msg = Decode(buffer).Substring(0, recv-1);
                    var code = GetCode(msg);
                    var data = GetData(msg);

                    switch (code)
                    {
                        case ResponseCode.GET_NAME:
                            UpdateName(data);
                            Helper.Send(_socket, "", ResponseCode.GET_CURRENT_CHANNEL);
                            break;
                        case ResponseCode.DISCONNECT: 
                            UpdateSystemLog("You've been disconnected.");
                            Disconnected();
                            return;
                        case ResponseCode.GET_CURRENT_CHANNEL:
                            UpdateChannel(data);
                            break;
                        case ResponseCode.SYSTEM_INFO:
                            UpdateSystemLog(data);
                            break;
                        case ResponseCode.CHANNEL_MSG:
                            UpdateChannelLog(data);
                            break;
                        case ResponseCode.PRIVATE_MSG:
                            UpdatePrivateLog(data);
                            break;
                    }
                }
            }
            catch (System.ComponentModel.InvalidAsynchronousStateException)
            {
                // Suppress InvalidAsynchronousStateException when trying to modify UI while closing application.
            }
            catch (System.Net.Sockets.SocketException)
            {
                UpdateSystemLog("Lost connection with server.");
                Disconnected();
            }
        }

        private void UpdateSystemLog(string data)
        {
            MethodInvoker inv = delegate
            {
                _form._chatUdpateLog += "\n# " + data;
            };

            _form.Invoke(inv);
        }

        private void UpdateChannelLog(string data)
        {
            MethodInvoker inv = delegate
            {
                _form._chatUdpateLog += "\n" + data;
            };

            _form.Invoke(inv);
        }

        private void UpdatePrivateLog(string data)
        {
            MethodInvoker inv = delegate
            {
                _form._chatUdpateLog += "\n> " + data;
            };

            _form.Invoke(inv);
        }

        private void Disconnected()
        {
            MethodInvoker inv = delegate
            {
                _form.label4.Text = "Disconnected";
                _form.label4.ForeColor = Color.Red;
                _form.button1.Enabled = true;
                _form.groupBox1.Enabled = _form.groupBox3.Enabled = _form.button2.Enabled = false;
            };

            _form.Invoke(inv);
        }

        private void UpdateName(string name) 
        {
            MethodInvoker inv = delegate
            {
                _form.textBox4.Text = name;
            };

            _form.Invoke(inv);
        }

        private void UpdateChannel(string channel)
        {
            MethodInvoker inv = delegate
            {
                _form.textBox5.Text = channel;
            };

            _form.Invoke(inv);
        }

        private string Decode(byte[] input)
        {
            return System.Text.Encoding.Default.GetString(input);
        }

        private byte[] Encode(string input)
        {
            return Encoding.UTF8.GetBytes(input);
        }

        private string GetCode(string input)
        {
            return input.Substring(1, 2);
        }

        private string GetData(string input)
        {
            return input.Substring(11, input.Length-12);
        }
    }
}
