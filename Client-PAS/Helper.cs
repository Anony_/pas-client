﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Client_PAS
{
    public static class Helper
    {
        public static void Send(Socket socket, string msg, string type)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                var hash = GetMd5Hash(md5Hash, msg).Substring(0, 6);
                var toSend = string.Format("{0}:{1}:{2}", type, hash, msg);

                socket.Send(Encoding.ASCII.GetBytes("{"+toSend+"}"+(char)1));
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
