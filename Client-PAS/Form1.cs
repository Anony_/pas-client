﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_PAS
{
    public partial class Form1 : Form
    {
        public string _chatUdpateLog = "";
        private Socket _socket;
        private Thread _thread;
        private Listener _listener;

        public Form1()
        {
            InitializeComponent();
        }

        private void UpdateChat_Tick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_chatUdpateLog)) 
                return;
            
            richTextBox1.AppendText(_chatUdpateLog);
            _chatUdpateLog = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            try 
            {
                var port = Convert.ToInt32(textBox2.Text);
                var ip = new byte[] {0, 0, 0, 0};

                var split = textBox1.Text.Split('.');

                if (split.Count() != 4)
                {
                    throw new ArgumentException("Given IP should have 4 octets.");
                }

                for (int i = 0; i < ip.Length; i++)
                {
                    ip[i] = Convert.ToByte(split[i], 10);
                }
                
                _socket.Connect(new IPEndPoint(new IPAddress(ip), port));

                label4.Text = "Connected";
                label4.ForeColor = Color.Green;
                button1.Enabled = false;
                groupBox1.Enabled = groupBox3.Enabled = button2.Enabled = true;

                // Starting listener thread
                _listener = new Listener(_socket, this);
                _thread = new Thread(new ThreadStart(_listener.Start));
                _thread.Start();
            }
            catch (FormatException) 
            {
                MessageBox.Show("Given IP/PORT is not valid!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Disconnect()
        {
            if (label4.Text == "Connected")
            {
                Helper.Send(_socket, "", ResponseCode.DISCONNECT);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void GetUsername()
        {
            Helper.Send(_socket, "", ResponseCode.GET_NAME);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox4.Text == "")
            {
                MessageBox.Show("Username cannot be empty.");
                return;
            }

            Helper.Send(_socket, textBox4.Text, ResponseCode.SET_NAME);
            textBox4.Text = "";

            GetUsername();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                MessageBox.Show("Channel name cannot be empty.");
                return;
            }

            Helper.Send(_socket, textBox5.Text, ResponseCode.JOIN_CHANNEL);
            textBox5.Text = "";
            Helper.Send(_socket, "", ResponseCode.GET_CURRENT_CHANNEL);
        }

        private void SendMsg(string msg)
        {
            if (msg == "")
            {
                MessageBox.Show("Send something else than empty string :)");
                return;
            }

            if (msg.StartsWith("/pm "))
            {
                var split = msg.Split(' ');

                if (!System.Text.RegularExpressions.Regex.IsMatch(split[1], "^[a-zA-Z0-9]+$"))
                {
                    MessageBox.Show("Invalid username!");
                    return;
                }

                Helper.Send(_socket, msg.Substring(4), ResponseCode.PRIVATE_MSG);
            }
            else
            {
                Helper.Send(_socket, msg, ResponseCode.CHANNEL_MSG);
            }

            textBox3.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SendMsg(textBox3.Text);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text == "") return;

            if (!System.Text.RegularExpressions.Regex.IsMatch(textBox4.Text, "^[a-zA-Z0-9]+$"))
            {
                MessageBox.Show("Username has to pass regex '^[a-zA-Z0-9]+$'.");

                var newUsername = "";

                foreach (var c in textBox4.Text)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(c.ToString(), "[a-zA-Z0-9]"))
                        newUsername += c;
                }

                textBox4.Text = newUsername;
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.Text == "") return;

            if (!System.Text.RegularExpressions.Regex.IsMatch(textBox5.Text, "^[a-zA-Z]+$"))
            {
                MessageBox.Show("Channel name has to pass regex '^[a-zA-Z]+$'.");

                var newChannel = "";

                foreach (var c in textBox5.Text)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(c.ToString(), "[a-zA-Z]"))
                        newChannel += c;
                }

                textBox5.Text = newChannel;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text == "") return;

            if (!System.Text.RegularExpressions.Regex.IsMatch(textBox3.Text, "^[0-9a-zA-Z,.:;'\"/\\<>\\[\\]{}|=+-_!#$%^&*()`~ ]+$"))
            {
                MessageBox.Show("Message has to pass regex '^[0-9a-zA-Z,.:;'\"/\\<>\\[\\]{}|=+-_!#$%^&*()`~ ]+$'.");

                var newChannel = "";

                foreach (var c in textBox3.Text)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(c.ToString(), "[0-9a-zA-Z,.:;'\"/\\<>\\[\\]{}|=+-_!#$%^&*()`~ ]"))
                        newChannel += c;
                }

                textBox3.Text = newChannel;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Disconnect();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                SendMsg(textBox3.Text);
                textBox3.Text = "";
                e.Handled = true;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Use \"/pm USER_NAME MESSAGE\" syntax.", "Helper");
        }
    }
}
