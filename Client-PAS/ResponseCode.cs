﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_PAS
{
    public static class ResponseCode
    {
        public const string SYSTEM_INFO = "00";
        public const string CHANNEL_MSG = "01";
        public const string PRIVATE_MSG = "02";
        public const string JOIN_CHANNEL = "03";
        public const string DISCONNECT = "04";
        public const string SET_NAME = "05";
        public const string GET_NAME = "06";
        public const string GET_CURRENT_CHANNEL = "07";
    }
}
